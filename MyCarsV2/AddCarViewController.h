//
//  AddCarViewController.h
//  MyCarsV2
//
//  Created by Orlando Gotera on 12/14/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface AddCarViewController : UIViewController
{
    AppDelegate *appDelegate;
    NSManagedObjectContext * context;
}
@property (strong) NSManagedObjectModel *aCar;

@end
