//
//  AppDelegate.h
//  MyCarsV2
//
//  Created by Orlando Gotera on 12/14/17.
//  Copyright © 2017 Orlando Gotera. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic) NSManagedObjectContext * context;

- (void)saveContext;


@end

